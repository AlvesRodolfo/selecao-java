package br.com.projectindra.projectindra.model;

import lombok.Data;

@Data
public class Mapa {
	private String key;
	private Double value;
}
