package br.com.projectindra.projectindra.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class ArqCsv {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String regiaoSigla;
	private String estadoSigla;
	private String municipio;
	private String revenda;
	private String produto;
	private String dataColeta;
	private Double valorCompra;
	private Double valorVenda;
	private String unidadeMedida;
	private String bandeira;
	
	public ArqCsv(String regiaoSigla, String estadoSigla, String municipio, String revenda, String produto,
			String dataColeta, Double valorCompra, Double valorVenda, String unidadeMedida, String bandeira) {
		super();
		this.regiaoSigla = regiaoSigla;
		this.estadoSigla = estadoSigla;
		this.municipio = municipio;
		this.revenda = revenda;
		this.produto = produto;
		this.dataColeta = dataColeta;
		this.valorCompra = valorCompra;
		this.valorVenda = valorVenda;
		this.unidadeMedida = unidadeMedida;
		this.bandeira = bandeira;
	}
}
