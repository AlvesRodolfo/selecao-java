package br.com.projectindra.projectindra.dal;

import java.util.List;

import br.com.projectindra.projectindra.model.ArqCsv;
import br.com.projectindra.projectindra.model.Mapa;

public interface ArqCsvDAL {

	List<ArqCsv> listarLinhas();
	ArqCsv salvarLinha(ArqCsv arquivo);
	List<ArqCsv> importarCsv();
	List<Mapa> mediaPrecoMunicipio();
	List<ArqCsv> infosSiglaRegiao(String sigla);
	List<ArqCsv> infosDistribuidora(String distribuidora);
	List<ArqCsv> infosDataDeColeta(String data);
	List<Mapa> infosMediaCompraVendaMunicipio();
	List<Mapa> infosMediaCompraVendaBandeira();
}
