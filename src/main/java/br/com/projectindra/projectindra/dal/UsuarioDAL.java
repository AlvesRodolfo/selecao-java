package br.com.projectindra.projectindra.dal;

import java.util.List;

import br.com.projectindra.projectindra.model.Usuario;

public interface UsuarioDAL {

	List<Usuario> listarUsuarios();
	Usuario salvarUsuario(Usuario usuario);
	Usuario atualizarUsuario(Usuario usuario);
	void deletarUsuario(Long id);
}
