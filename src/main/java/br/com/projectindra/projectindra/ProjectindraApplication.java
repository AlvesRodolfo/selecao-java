package br.com.projectindra.projectindra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ProjectindraApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectindraApplication.class, args);	
	}
	
	@GetMapping("/")
	public String home(){
		return "<html><h1>TELA PRINCIPAL</h1></html>";
	}

}