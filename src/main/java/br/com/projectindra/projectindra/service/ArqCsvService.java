package br.com.projectindra.projectindra.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projectindra.projectindra.dal.ArqCsvDAL;
import br.com.projectindra.projectindra.model.ArqCsv;
import br.com.projectindra.projectindra.model.Mapa;
import br.com.projectindra.projectindra.repository.ArqCsvRepository;

@Service
public class ArqCsvService implements ArqCsvDAL{

	@Autowired
	private ArqCsvRepository repository;
	
	@Override
	public List<ArqCsv> listarLinhas() {
		return repository.findAll();
	}
	
	@Override
	public ArqCsv salvarLinha(ArqCsv arquivo) {
		return repository.save(arquivo);
	}

	@Override
	public List<ArqCsv> importarCsv() {
		try {
			String sourceFileStr = "C:\\Users\\rodolfo\\Documents\\CSV\\d2.csv";		
			try (BufferedReader br = new BufferedReader(new FileReader(sourceFileStr))) {

				String itemCsv = br.readLine();
				int cont = 0;

				while (itemCsv != null) {
					if (cont == 0) {
						cont++;
						itemCsv = br.readLine();
					}else{
						String[] fields = itemCsv.split("	");
						String regiaoSigla = fields[0];
						String estadoSigla = fields[1];
						String municipio = fields[2];
						String revenda = fields[3];
						String produto = fields[4];
						String dataColeta = fields[5];
						Double valorCompra = 0.0;
						if(fields[6].equals("")){
						}else{
							valorCompra = Double.parseDouble(fields[6].replace(",", "."));						
						}
						Double valorVenda = 0.0;
						if(fields[7].equals("")){
						}else{
							valorVenda = Double.parseDouble(fields[7].replace(",", "."));						
						}
						String unidadeMedida = fields[8];
						String bandeira = fields[9];

						repository.save(new ArqCsv(regiaoSigla, estadoSigla, municipio, revenda, 
								produto, dataColeta, valorCompra, valorVenda, 
								unidadeMedida, bandeira));

						itemCsv = br.readLine();
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	@Override
	public List<Mapa> mediaPrecoMunicipio() {
		return mediaPrecoCombustivelMunicipio(repository.findAll());
	}
	
	public static List<Mapa> mediaPrecoCombustivelMunicipio(List<ArqCsv> linhas){
		List<String> nomesMunicipios = new ArrayList<String>();
		List<Double> valoresPostos = new ArrayList<Double>();
		Map<String, Integer> municipioQtd = new HashMap<String, Integer>();
		Map<String, Double> postoValor = new HashMap<String, Double>();
		List<Mapa> valoresFinais = new ArrayList<Mapa>();
		double valor = 0;
		double media = 0;
		
		for (ArqCsv municipio : linhas) {
			nomesMunicipios.add(municipio.getMunicipio());
		}
		for (String municipio : nomesMunicipios) {
			municipioQtd.put(municipio, Collections.frequency(nomesMunicipios, municipio));
		}
		
		for (Entry<String, Integer> municipio : municipioQtd.entrySet()) {
			for (ArqCsv posto : linhas) {
				if (municipio.getKey().equals(posto.getMunicipio())) {
					valoresPostos.add(posto.getValorVenda());
				}
			}
			for (Double valores : valoresPostos) {
				valor += valores;
			}
			postoValor.put(municipio.getKey(), valor);
			valoresPostos.clear();
			valor=0;
		}
		
		for (Entry<String, Integer> municipio : municipioQtd.entrySet()){
			for (Entry<String, Double> totalPreco : postoValor.entrySet()){
				if (municipio.getKey().equals(totalPreco.getKey())) {
					media = totalPreco.getValue() / municipio.getValue();
					Mapa mapa = new Mapa();
					mapa.setKey(municipio.getKey());
					mapa.setValue(media);
					valoresFinais.add(mapa);
				}
			}
		}
	
		return valoresFinais;
	}

	@Override
	public List<ArqCsv> infosSiglaRegiao(String sigla) {
		return informacoesPorSiglaRegiao(repository.findAll(),sigla);
	}
	
	//Informacoes por sigla da regiao
	public static List<ArqCsv> informacoesPorSiglaRegiao(List<ArqCsv> linhas, String siglaRegiao){
		List<ArqCsv> listaPorRegiao = new ArrayList<ArqCsv>();

		for (ArqCsv linha : linhas) {
			if (linha.getRegiaoSigla().equals(siglaRegiao)) {
				listaPorRegiao.add(new ArqCsv(linha.getRegiaoSigla(), linha.getEstadoSigla(), linha.getMunicipio(), linha.getRevenda(), 
						linha.getProduto(), linha.getDataColeta(), linha.getValorCompra(), linha.getValorVenda(), 
						linha.getUnidadeMedida(), linha.getBandeira()));
			}
		}
		return listaPorRegiao;
	}

	@Override
	public List<ArqCsv> infosDistribuidora(String distribuidora) {
		return informacoesPorDistribuidora(repository.findAll(),distribuidora);
	}
	
	//Informacoes por distrubidora
	public static List<ArqCsv> informacoesPorDistribuidora(List<ArqCsv> linhas, String distribuidora){
		List<ArqCsv> listaDistribuidora = new ArrayList<ArqCsv>();

		for (ArqCsv linha : linhas) {
			if (linha.getRevenda().equals(distribuidora)) {
				listaDistribuidora.add(new ArqCsv(linha.getRegiaoSigla(), linha.getEstadoSigla(), linha.getMunicipio(), linha.getRevenda(), 
						linha.getProduto(), linha.getDataColeta(), linha.getValorCompra(), linha.getValorVenda(), 
						linha.getUnidadeMedida(), linha.getBandeira()));
			}
		}
		return listaDistribuidora;
	}

	@Override
	public List<ArqCsv> infosDataDeColeta(String data) {
		return informacoesPorDataDaColeta(repository.findAll(),data);
	}
	
	public static List<ArqCsv> informacoesPorDataDaColeta(List<ArqCsv> linhas, String data){
		String replaceData = data.replace("-", "/");
		List<ArqCsv> listDataColeta = new ArrayList<ArqCsv>();

		for (ArqCsv linha : linhas) {
			if (linha.getDataColeta().equals(replaceData)) {
				listDataColeta.add(new ArqCsv(linha.getRegiaoSigla(), linha.getEstadoSigla(), linha.getMunicipio(), linha.getRevenda(), 
						linha.getProduto(), linha.getDataColeta(), linha.getValorCompra(), linha.getValorVenda(), 
						linha.getUnidadeMedida(), linha.getBandeira()));
			}
		}
		return listDataColeta;
	}
	
	@Override
	public List<Mapa> infosMediaCompraVendaMunicipio() {
		return mediaPrecoCompraPrecoVendaMunicipio(repository.findAll());
	}
	
	public static List<Mapa> mediaPrecoCompraPrecoVendaMunicipio(List<ArqCsv> linhas){
		List<String> nomesMunicipios = new ArrayList<String>();
		List<Double> valoresComprasPostos = new ArrayList<Double>();
		List<Double> valoresVendasPostos = new ArrayList<Double>();
		Map<String, Integer> municipiosQtd = new HashMap<String, Integer>();
		Map<String, Double> postosCompra = new HashMap<String, Double>();
		Map<String, Double> postosVenda = new HashMap<String, Double>();
		List<Mapa> valoresFinais = new ArrayList<Mapa>();
		double valorCompra = 0;
		double valorVenda = 0;
		double mediaCompra = 0;
		double mediaVenda = 0;

		for (ArqCsv linha : linhas) {
			nomesMunicipios.add(linha.getMunicipio());
		}
		for (String municipio : nomesMunicipios) {
			municipiosQtd.put(municipio, Collections.frequency(nomesMunicipios, municipio));
		}

		for (Entry<String, Integer> municipio : municipiosQtd.entrySet()) {
			for (ArqCsv linha : linhas) {
				if (municipio.getKey().equals(linha.getMunicipio())) {
					valoresComprasPostos.add(linha.getValorCompra());
					valoresVendasPostos.add(linha.getValorVenda());
				}
			}
			for (Double valores : valoresComprasPostos) {
				valorCompra += valores;
			}
			for (Double valores : valoresVendasPostos) {
				valorVenda += valores;
			}
			postosCompra.put(municipio.getKey(), valorCompra);
			postosVenda.put(municipio.getKey(), valorVenda);
			valoresComprasPostos.clear();
			valoresVendasPostos.clear();
			valorCompra=0;
			valorVenda=0;
		}

		for (Entry<String, Integer> municipio : municipiosQtd.entrySet()){
			for (Entry<String, Double> totalCompra : postosCompra.entrySet()){
				if (municipio.getKey().equals(totalCompra.getKey())) {
					mediaCompra = totalCompra.getValue() / municipio.getValue();
					Mapa mapa = new Mapa();
					mapa.setKey(municipio.getKey());
					mapa.setValue(mediaCompra);
					valoresFinais.add(mapa);
				}
			}
			for (Entry<String, Double> totalVenda : postosVenda.entrySet()){
				if (municipio.getKey().equals(totalVenda.getKey())) {
					mediaVenda = totalVenda.getValue() / municipio.getValue();
					Mapa mapa = new Mapa();
					mapa.setKey(municipio.getKey());
					mapa.setValue(mediaVenda);
					valoresFinais.add(mapa);
				}
			}
		}

		return valoresFinais;
	}

	@Override
	public List<Mapa> infosMediaCompraVendaBandeira() {
		return mediaPrecoCompraPrecoVendaBandeira(repository.findAll());
	}

	//Preco medio da venda e da compra por bandeira
	public static List<Mapa> mediaPrecoCompraPrecoVendaBandeira(List<ArqCsv> linhas){
		List<String> nomesBandeiras = new ArrayList<String>();
		List<Double> valoresComprasPostos = new ArrayList<Double>();
		List<Double> valoresVendasPostos = new ArrayList<Double>();
		Map<String, Integer> bandeiraQtd = new HashMap<String, Integer>();
		Map<String, Double> postosCompra = new HashMap<String, Double>();
		Map<String, Double> postosVenda = new HashMap<String, Double>();
		List<Mapa> valoresFinais = new ArrayList<Mapa>();
		double valorCompra = 0;
		double valorVenda = 0;
		double mediaCompra = 0;
		double mediaVenda = 0;

		for (ArqCsv bandeiras : linhas) {
			nomesBandeiras.add(bandeiras.getBandeira());
		}
		for (String bandeira : nomesBandeiras) {
			bandeiraQtd.put(bandeira, Collections.frequency(nomesBandeiras, bandeira));
		}

		for (Entry<String, Integer> bandeira : bandeiraQtd.entrySet()) {
			for (ArqCsv posto : linhas) {
				if (bandeira.getKey().equals(posto.getBandeira())) {
					valoresComprasPostos.add(posto.getValorCompra());
					valoresVendasPostos.add(posto.getValorVenda());
				}
			}
			for (Double valores : valoresComprasPostos) {
				valorCompra += valores;
			}
			for (Double valores : valoresVendasPostos) {
				valorVenda += valores;
			}
			postosCompra.put(bandeira.getKey(), valorCompra);
			postosVenda.put(bandeira.getKey(), valorVenda);
			valoresComprasPostos.clear();
			valoresVendasPostos.clear();
			valorCompra=0;
			valorVenda=0;
		}

		for (Entry<String, Integer> bandeira : bandeiraQtd.entrySet()){
			for (Entry<String, Double> totalCompra : postosCompra.entrySet()){
				if (bandeira.getKey().equals(totalCompra.getKey())) {
					mediaCompra = totalCompra.getValue() / bandeira.getValue();
					Mapa mapa = new Mapa();
					mapa.setKey(bandeira.getKey());
					mapa.setValue(mediaCompra);
					valoresFinais.add(mapa);
				}
			}
			for (Entry<String, Double> totalVenda : postosVenda.entrySet()){
				if (bandeira.getKey().equals(totalVenda.getKey())) {
					mediaVenda = totalVenda.getValue() / bandeira.getValue();
					Mapa mapa = new Mapa();
					mapa.setKey(bandeira.getKey());
					mapa.setValue(mediaVenda);
					valoresFinais.add(mapa);
				}
			}
		}

		return valoresFinais;
	}
}
