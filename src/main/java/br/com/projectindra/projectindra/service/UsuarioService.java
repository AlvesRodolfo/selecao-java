package br.com.projectindra.projectindra.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projectindra.projectindra.dal.UsuarioDAL;
import br.com.projectindra.projectindra.model.Usuario;
import br.com.projectindra.projectindra.repository.UsuarioRepository;

@Service
public class UsuarioService implements UsuarioDAL{

	@Autowired
	private UsuarioRepository repository;
	
	@Override
	public List<Usuario> listarUsuarios() {
		return repository.findAll();
	}
	
	@Override
	public Usuario salvarUsuario(Usuario usuario) {
		if (usuario!=null) {
			return repository.save(usuario);			
		}
		return null;
	}

	@Override
	public Usuario atualizarUsuario(Usuario usuario) {
		if(usuario != null && repository.existsById(usuario.getId())){
			return repository.save(usuario);
		}
		return null;
	}

	@Override
	public void deletarUsuario(Long id) {
		if(repository.existsById(id)){
			repository.deleteById(id);
		}
	}
}
