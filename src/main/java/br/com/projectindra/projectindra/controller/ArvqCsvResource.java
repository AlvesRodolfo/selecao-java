package br.com.projectindra.projectindra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.projectindra.projectindra.model.ArqCsv;
import br.com.projectindra.projectindra.model.Mapa;
import br.com.projectindra.projectindra.service.ArqCsvService;

@RestController
@RequestMapping("/arquivocsv")
public class ArvqCsvResource {

	@Autowired
	private ArqCsvService service;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<ArqCsv> listarDados(){
		return service.listarLinhas();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ArqCsv inserirDados(@RequestBody ArqCsv arquivo){
		return service.salvarLinha(arquivo);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/importar")
	public List<ArqCsv> importarDados() {
		return service.importarCsv();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info-media-combustivel-municipio")
	public List<Mapa> mediaPrecoCombustivel() {
		return service.mediaPrecoMunicipio();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info-sigla-regiao/{sigla}")
	public List<ArqCsv> infosSiglaRegiao(@PathVariable String sigla) {
		return service.infosSiglaRegiao(sigla);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info-distribuidora/{distribuidora}")
	public List<ArqCsv> infosDistribuidora(@PathVariable String distribuidora) {
		return service.infosDistribuidora(distribuidora);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info-data-coleta/{data}")
	public List<ArqCsv> infosDataDeColeta(@PathVariable String data) {
		return service.infosDataDeColeta(data);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info-media-compra-venda-municipio")
	public List<Mapa> infosMediaCompraVendaMunicipio() {
		return service.infosMediaCompraVendaMunicipio();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info-media-compra-venda-bandeira")
	public List<Mapa> infosMediaCompraVendaBandeira() {
		return service.infosMediaCompraVendaBandeira();
	}
}
