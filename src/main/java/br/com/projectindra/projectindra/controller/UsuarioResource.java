package br.com.projectindra.projectindra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.projectindra.projectindra.model.Usuario;
import br.com.projectindra.projectindra.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

	@Autowired
	private UsuarioService service;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Usuario> listarTodos(){
		return service.listarUsuarios();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public Usuario inserirUsuario(@RequestBody Usuario usuario){
		return service.salvarUsuario(usuario);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public Usuario atualizarUsuario(@RequestBody Usuario usuario){
		return service.atualizarUsuario(usuario);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deletarUsuario(@PathVariable Long id){
		service.deletarUsuario(id);
	}
}
