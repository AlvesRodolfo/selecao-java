package br.com.projectindra.projectindra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.projectindra.projectindra.model.ArqCsv;

public interface ArqCsvRepository extends JpaRepository<ArqCsv, Long> {

}
