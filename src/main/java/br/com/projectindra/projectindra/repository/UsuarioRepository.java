package br.com.projectindra.projectindra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.projectindra.projectindra.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
